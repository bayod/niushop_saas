<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\shop\controller;

use app\model\upload\Upload as UploadModel;
use app\model\system\User as UserModel;
use app\model\upload\Config as ConfigModel;
use app\model\upload\Album as AlbumModel;
use think\Exception;
use app\model\BaseModel;

/**
 * 图片上传
 * Class Verify
 * @package app\shop\controller
 */
class Upload extends BaseShop
{
    public $site_id = 0;
    protected $app_module = "shop";

    public function __construct()
    {
        //执行父类构造函数
        parent::__construct();
        $this->site_id = request()->siteid();
        if (empty($this->site_id)) {
            $this->site_id = input("site_id", 0);
            request()->siteid($this->site_id);
        }
    }

    /**
     * 上传(不存入相册)
     * @return \app\model\upload\Ambigous|\multitype
     */
    public function image()
    {
        $upload_model = new UploadModel($this->site_id, $this->app_module);
        $thumb_type = input("thumb", "");
        $name = input("name", "");
        $watermark = input("watermark", 0); // 是否需生成水印
        $cloud = input("cloud", 1); // 是否需上传到云存储
        $param = array (
            "thumb_type" => "",
            "name" => "file",
            "watermark" => $watermark,
            "cloud" => $cloud
        );
        $result = $upload_model->setPath("common/images/" . date("Ymd") . '/')->image($param);
        return $result;
    }

    /**
     * 上传 存入相册
     * @return \multitype
     */
    public function album()
    {
        $upload_model = new UploadModel($this->site_id);
        $album_id = input("album_id", 0);
        $name = input("name", "");
        $param = array (
            "thumb_type" => [ "BIG", "MID", "SMALL" ],
            "name" => "file",
            "album_id" => $album_id
        );
        $result = $upload_model->setPath("common/images/" . date("Ymd") . '/')->imageToAlbum($param);
        return $result;
    }

    /**
     * 视频上传
     * @return \multitype
     */
    public function video()
    {
        $upload_model = new UploadModel($this->site_id);
        $name = input("name", "");
        $param = array (
            "name" => "file"
        );
        $result = $upload_model->setPath("common/video/" . date("Ymd") . '/')->video($param);
        return $result;
    }

    /**
     * 上传(不存入相册)
     * @return \app\model\upload\Ambigous|\multitype
     */
    public function upload()
    {
        $upload_model = new UploadModel();
        $thumb_type = input("thumb", "");
        $name = input("name", "");
        $param = array (
            "thumb_type" => "",
            "name" => "file"
        );
        $result = $upload_model->setPath("common/images/" . date("Ymd") . '/')->image($param);
        return $result;
    }

    /**
     *  校验文件
     */
    public function checkfile()
    {
        $upload_model = new UploadModel();
        $result = $upload_model->domainCheckFile([ "name" => "file" ]);
        return $result;
    }

    /**
     * 上传文件
     */
    public function file()
    {
        $upload_model = new UploadModel($this->site_id);

        $param = array (
            "name" => "file",
            'extend_type' => [ 'xlsx' ]
        );

        $result = $upload_model->setPath("common/file/" . date("Ymd") . '/')->file($param);
        return $result;
    }

    /**
     * 删除文件
     */
    public function deleteFile()
    {
        if (request()->isAjax()) {
            $path = input("path", '');
            $res = false;
            if (!empty($path)) {
                $res = delFile($path);
            }
            return $res;
        }
    }

    /**
     * 上传微信支付证书
     */
    public function uploadWechatCert()
    {
        $upload_model = new UploadModel();
        $site_id = request()->siteid();
        $name = input("name", "");
        $extend_type = ['pem'];
        $param = array(
            "name" => "file",
            "extend_type" => $extend_type
        );

        $site_id = $site_id > 0 ? $site_id : 0;
        $result = $upload_model->setPath("common/wechat/cert/".$site_id."/")->file($param);
        return $result;
    }
    
    /*
     * 替换图片文件
     * */
    public function modifyFile()
    {

//      实例化响应数据结构生成类
        $base_model = new BaseModel();

        try {
//            参数
            $album_id = input("album_id", '');
            $pic_id = input("pic_id", '');

//            获取图片信息
            $album_model = new AlbumModel($this->site_id);
            $get_pic_info = array(
                ["pic_id", "=", $pic_id],
                ["site_id", "=", $this->site_id],
            );

//            图片信息
            $pic_info = $album_model->getAlbumPicInfo($get_pic_info);
//            判断是否找到有效图片
            if(empty($pic_info) || empty($pic_info['data'])){
                return json($base_model->error('', 'FAIL'));
            }

//            文件名及后缀
            $file_full_name = basename($pic_info['data']['pic_path']);
            $filename_arr = explode('.', $file_full_name);
            $filename =$filename_arr[0];
            $suffix = $filename_arr[1];

//            实例化文件上传类
            $upload_model = new UploadModel($this->site_id);

            $upload_param = array (
                "name" => "file",
                "album_id" => $album_id,
                "pic_id" => $pic_id,
                "thumb_type" => [ "BIG", "MID", "SMALL" ],
                "filename" => $filename,
                "suffix" => $suffix
            );


            $result = $upload_model->setPath("common/images/" . date("Ymd") . '/')->modifyFile($upload_param);

            return json($result);

        }catch (Exception $e){
            return json($base_model->error($e, 'FAIL'));
        }

    }
}