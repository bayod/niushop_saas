<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\shop\controller;

use app\model\goods\Config as GoodsConfigModel;
use app\model\system\Pay;
use app\model\web\Config as ConfigModel;
use app\model\system\Servicer as ServicerModel;

/**
 * 设置 控制器
 */
class Config extends BaseShop
{
    public function copyright()
    {
        $config_model = new ConfigModel();
        $copyright = $config_model->getCopyright($this->site_id, $this->app_module);
        if (request()->isAjax()) {
            $logo = input('logo', '');
            $data = [
                'logo' => $logo ? : '',
                'company_name' => input('company_name', ''),
                'copyright_link' => input('copyright_link', ''),
                'copyright_desc' => input('copyright_desc', ''),
            ];
            $this->addLog("修改版权配置");
            $res = $config_model->setCopyright($data, $this->site_id, $this->app_module);
            return $res;
        }
        $this->assign('copyright_config', $copyright[ 'data' ][ 'value' ]);
        return $this->fetch('config/copyright');
    }

    /**
     * 支付管理
     */
    public function pay()
    {
        if (request()->isAjax()) {
            $pay_model = new Pay();
            $list = $pay_model->getPayType([]);
            return $list;
        } else {
            return $this->fetch('config/pay');
        }
    }

    /**
     * 默认图设置
     */
    public function defaultPicture()
    {
        $upload_config_model = new ConfigModel();
        if (request()->isAjax()) {
            $data = array (
                "default_goods_img" => input("default_goods_img", ""),
                "default_headimg" => input("default_headimg", ""),
		        "default_storeimg" => input("default_storeimg", ""),
            );
            $this->addLog("修改默认图配置");
            $res = $upload_config_model->setDefaultImg($data, $this->site_id, $this->app_module);

            return $res;
        } else {
            $upload_config_result = $upload_config_model->getDefaultImg($this->site_id, $this->app_module);
            $this->assign("default_img", $upload_config_result[ 'data' ][ 'value' ]);
            return $this->fetch('config/default_picture');
        }
    }

    /*
     * 售后保障
     */
    public function aftersale()
    {
        $goods_config_model = new GoodsConfigModel();
        if (request()->isAjax()) {
            $content = input('content', '');//售后保障协议
            $is_display = input('is_display', '1');//售后保障协议是否显示1为显示0为不显示
            return $goods_config_model->setAfterSaleConfig('售后保障协议', $content, $this->site_id,$is_display);
        } else {
            $content = $goods_config_model->getAfterSaleConfig($this->site_id);
            $this->assign('content', $content);
            return $this->fetch('config/aftersale');
        }
    }

    /**
     * 客服配置
     */
    public function servicer()
    {
        $servicer_model = new ServicerModel();
        if (request()->isAjax()) {
            $system     = input('system', 0);//是否启用Niushop客服
            $weapp      = input('weapp', 0);//是否启用小程序客服
            $open       = input('open', 0);//是否启用第三方客服H5
            $open_pc    = input('open_pc', 0);//是否启用第三方客服PC
            $open_url   = input('open_url', '');//第三方客服 链接
            $socket_url = input('socket_url', '');// websocket 链接

            $data = [
                'system'     => $system,
                'weapp'      => $weapp,
                'open'       => $open,
                'open_pc'    => $open_pc,
                'open_url'   => $open_url,
                'socket_url' => $socket_url,
            ];

            return $servicer_model->setServicerConfig($data, $this->site_id, $this->app_module);
        } else {
            $config = $servicer_model->getServicerConfig($this->site_id, $this->app_module)['data'] ?? [];
            $this->assign('config', $config['value'] ?? []);
            $this->assign('pc_is_exit', addon_is_exit('pc', $this->site_id));
            return $this->fetch('config/servicer');
        }

    }
}