<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\shop\controller;

use app\model\system\SysOrder as SysOrderModel;
use app\model\message\SmsPackage as SmsPackageModel;


class Smspackage extends BaseShop
{
	/**
	 * 订单购买页
	 */
	public function payment()
	{
		
		$sms_package_model = new SmsPackageModel();
		$list_result = $sms_package_model->getSmsPackageList([ [ 'status', '=', 1 ] ]);
		if(empty($list_result["data"])){
		    $this->error("平台未设置短信套餐");
        }
		$this->assign('list', $list_result["data"]);
		
		$this->assign("menu_info", [ 'title' => "短信套餐" ]);
		return $this->fetch('smspackage/payment');
	}
	
	/**
	 * 是否存在待支付的订单
	 */
	public function checkOrder()
	{
		$sys_order_model = new SysOrderModel();
		$condition = array(
			[ "pay_status", "=", 0 ],
			[ "buyer_uid", '=', $this->sys_uid ],
			[ "order_type", '=', 'sms' ],
		);
		$result = $sys_order_model->getOrderInfo($condition, 'out_trade_no');
		return $result;
	}
}