<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\admin\controller;

use app\Controller;
use app\model\system\Addon;
use app\model\system\Menu;
use app\model\system\User as UserModel;
use app\model\web\Config as ConfigModel;
use app\model\web\WebSite;

/**
 *
 * Class BaseAdmin
 * 版本 1.0.6
 */
class BaseAdmin extends Controller
{
	
	protected $init_menu = [];
	protected $crumbs = [];
	protected $crumbs_array = [];
	
	protected $uid;
	protected $sys_user_info;
	protected $url;
	protected $menus;
	protected $site_id = 0;
	protected $app_module = 'admin';
	protected $addon = '';
	
	public function __construct()
	{
		//执行父类构造函数
		parent::__construct();
		request()->siteid(0);
		$user = new UserModel();
		//检测基础登录
		$this->uid = $user->uid($this->app_module);
		
		$this->url = request()->parseUrl();
		$this->addon = request()->addon() ? request()->addon() : '';
		$this->user_info = $user->userInfo($this->app_module);
		$this->assign("user_info", $this->user_info);
		$this->checkLogin();
		
		if (!request()->isAjax()) {
			//获取菜单
			$this->menus = $this->getMenuList();
			$this->initBaseInfo();
		}
	}
	
	/**
	 * 加载基础信息
	 */
	private function initBaseInfo()
	{
		//获取一级权限菜单
		$this->getTopMenu();
		$menu_model = new Menu();
		$info = $menu_model->getMenuInfoByUrl($this->url, $this->app_module, $this->addon);
		if (!empty($info['data'])) {
			$this->getParentMenuList($info['data']['name']);
			$this->assign("menu_info", $info['data']);
		}
		
		//加载网站基础信息
		$website = new WebSite();
		$website_info = $website->getWebSite([ [ 'site_id', '=', 0 ] ], 'title,logo,desc,keywords,web_status,close_reason');
		$this->assign("website", $website_info['data']);
		//加载菜单树
		$init_menu = $this->initMenu($this->menus, '');
		
		// 应用下的菜单特殊处理
		if (!empty($this->crumbs) && $this->crumbs[0]['name'] == 'TOOL_ROOT') {
			
			//如果当前选择了【应用管理】，则只保留【应用管理】菜单
			if ($this->crumbs[1]['name'] == 'PROMOTION_TOOL') {
				foreach ($init_menu as $k => $v) {
					if ($v['selected']) {
						$init_menu[ $k ]['child_list'] = [ $v['child_list']['PROMOTION_TOOL'] ];
						break;
					}
				}
			} else {
				//选择了应用下的某个插件，则移除【应用管理】菜单，显示该插件下的菜单，并且标题名称改为插件名称
				$addon_model = new Addon();
				$addon_info = $addon_model->getAddonInfo([ [ 'name', '=', request()->addon() ] ], 'name,title');
				$addon_info = $addon_info['data'];
				foreach ($init_menu as $k => $v) {
					if ($v['selected']) {
						$this->crumbs[0]['title'] = $addon_info['title'];
						unset($init_menu[ $k ]['child_list']['PROMOTION_TOOL']);
						foreach ($init_menu[ $k ]['child_list'] as $ck => $cv) {
							if ($cv['addon'] != $addon_info['name']) {
								unset($init_menu[ $k ]['child_list'][ $ck ]);
							}
						}
						break;
					}
				}
			}
		}
		
		//加载版权信息
		$config_model = new ConfigModel();
		$copyright = $config_model->getCopyright();
		$this->assign('copyright', $copyright['data']['value']);
		$this->assign("url", $this->url);
		$this->assign("menu", $init_menu);
		
		$this->assign("crumbs", $this->crumbs);
		
	}
	
	/**
	 * layui化处理菜单数据
	 */
	public function initMenu($menus_list, $parent = "")
	{
		$temp_list = [];
		if (!empty($menus_list)) {
			foreach ($menus_list as $menu_k => $menu_v) {
				if (in_array($menu_v['name'], $this->crumbs_array)) {
					$selected = true;
				} else {
					$selected = false;
				}
				
				if ($menu_v["parent"] == $parent && $menu_v["is_show"] == 1) {
					$temp_item = array(
						'addon' => $menu_v['addon'],
						'selected' => $selected,
						'url' => addon_url($menu_v['url']),
						'title' => $menu_v['title'],
						'icon' => $menu_v['picture'],
						'icon_selected' => $menu_v['picture_select'],
						'target' => ''
					);
					
					$child = $this->initMenu($menus_list, $menu_v["name"]);//获取下级的菜单
					$temp_item["child_list"] = $child;
					$temp_list[ $menu_v["name"] ] = $temp_item;
				}
			}
		}
		return $temp_list;
	}
	
	/**
	 * 获取上级菜单列表
	 * @param number $menu_id
	 */
	private function getParentMenuList($name = '')
	{
		if (!empty($name)) {
			$menu_model = new Menu();
			$menu_info_result = $menu_model->getMenuInfo([ [ 'name', "=", $name ], [ 'app_module', '=', $this->app_module ] ]);
			$menu_info = $menu_info_result["data"];
			if (!empty($menu_info)) {
				$this->getParentMenuList($menu_info['parent']);
				$menu_info["url"] = addon_url($menu_info["url"]);
				$this->crumbs[] = $menu_info;
				$this->crumbs_array[] = $menu_info['name'];
			}
		}
		
	}
	
	/**
	 * 验证登录
	 */
	private function checkLogin()
	{
		//验证基础登录
		if (!$this->uid) {
			$this->redirect(url('admin/login/login'));
		}
	}
	
	/**
	 * 获取菜单
	 */
	private function getMenuList()
	{
		$menu_model = new Menu();
		$menus = $menu_model->getMenuList([ [ 'app_module', "=", $this->app_module ], [ 'is_show', "=", 1 ] ], '*', 'sort asc');
		return $menus['data'];
	}
	
	/**
	 * 获取顶级菜单
	 */
	protected function getTopMenu()
	{
		$list = array_filter($this->menus, function ($v) {
			return $v['parent'] == '0';
		});
		return $list;
		
	}
	
	/**
	 * 四级菜单
	 * @param unknown $params
	 */
	protected function forthMenu($params = [])
	{
		$url = strtolower($this->url);
		$menu_model = new Menu();
		$menu_info = $menu_model->getMenuInfo([ [ 'url', "=", $url ], [ 'level', '=', 4 ] ], 'parent');
		if (!empty($menu_info['data'])) {
			$menus = $menu_model->getMenuList([ [ 'app_module', "=", $this->app_module ], [ 'is_show', "=", 1 ], [ 'parent', '=', $menu_info['data']['parent'] ] ], '*', 'sort asc');
			foreach ($menus['data'] as $k => $v) {
				$menus['data'][ $k ]['parse_url'] = addon_url($menus['data'][ $k ]['url'], $params);
				if ($menus['data'][ $k ]['url'] == $url) {
					$menus['data'][ $k ]['selected'] = 1;
				} else {
					$menus['data'][ $k ]['selected'] = 0;
				}
			}
			$this->assign('forth_menu', $menus['data']);
		}
	}
	
	/**
	 * 添加日志
	 * @param string $action_name
	 * @param array $data
	 */
	protected function addLog($action_name, $data = [])
	{
		$user = new UserModel();
		$user->addUserLog($this->uid, $this->user_info['username'], $this->site_id, $action_name, $data);
	}
}