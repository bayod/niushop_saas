<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\admin\controller;

use app\model\system\SysOrder as SysOrderModel;
use app\model\system\SysOrderStat;
use app\model\system\Site;
use think\facade\Db;

/**
 * 订单 控制器
 * 版本 1.0.6
 */
class Sysorder extends BaseAdmin
{
	
	/**
	 * 订单概况
	 */
	public function index()
	{
		$sys_order_stat_model = new SysOrderStat();
		//订单总数
		$condition = array(
			[ 'order_status', 'not in', [ -1, 0 ] ],
			[ 'pay_status', '=', 1 ]
		);
        //店铺套餐订单总额
        $site_condition = array_merge($condition, [ [ 'order_type', 'in', 'group,sms' ] ]);
		$site_order_sum_result = $sys_order_stat_model->getOrderSum($site_condition, 'order_money');
		$site_order_sum = $site_order_sum_result['data'];
		$this->assign('site_order_sum', sprintf('%.2f', round(floor($site_order_sum * 100) / 100, 2)));

        //套餐订单总额
		$group_condition = array_merge($condition, [ [ 'order_type', '=', 'group' ] ]);
		$group_order_sum_result = $sys_order_stat_model->getOrderSum($group_condition, 'order_money');
		$group_order_sum = $group_order_sum_result['data'];
		$this->assign('group_order_sum', $group_order_sum);

        //店铺总数
        $site_model = new Site();
        $site_count = $site_model->getSiteCount([]);
        $this->assign('site_count', $site_count['data']);

        //短信订单总额
		$sms_condition = array_merge($condition, [ [ 'order_type', '=', 'sms' ] ]);
		$sms_order_sum_result = $sys_order_stat_model->getOrderSum($sms_condition, 'order_money');
		$sms_order_sum = $sms_order_sum_result['data'];
        $this->assign('sms_order_sum', $sms_order_sum);


        //店铺成交额(续费，新增)

        //续费
        $renew_condition = array_merge($condition, [ [ 'order_type', '=', 'group' ],['is_renew','=',1] ]);
        $renew_site_order_sum_result = $sys_order_stat_model->getOrderSum($renew_condition, 'order_money');
        $site_renew_order_sum = $renew_site_order_sum_result['data'];
        $this->assign('site_renew_order_sum', sprintf('%.2f', round(floor($site_renew_order_sum * 100) / 100, 2)));

        //新增
        $new_condition = array_merge($condition, [ [ 'order_type', '=', 'group' ],['is_renew','=',0] ]);
        $new_site_order_sum_result = $sys_order_stat_model->getOrderSum($new_condition, 'order_money');
        $site_new_order_sum = $new_site_order_sum_result['data'];
        $this->assign('site_new_order_sum', sprintf('%.2f', round(floor($site_new_order_sum * 100) / 100, 2)));

        //店铺总数（试用，购买）

        //试用
        $try_site_count = $site_model->getSiteCount([ [ 'is_try','=',1 ] ]);
        $this->assign('try_site_count', $try_site_count['data']);

        //购买
        $buy_site_count = $site_model->getSiteCount([ [ 'is_try','=',0 ] ]);
        $this->assign('buy_site_count', $buy_site_count['data']);


        //近十天的短信订单数 和 套餐订单数
        $group_order_data = [];
        $sms_order_data = [];
        $group_order_money = [];
        $sms_order_money = [];
        $date_day = getweeks();
        $start_date = date_to_time(date('Y-m-d', strtotime('-10 days')));
        $group_condition = array_merge($condition, [ [ 'order_type', 'in', 'group,sms' ], ['create_time','>', $start_date]]);
        $group_order_list = $sys_order_stat_model->getOrderList($group_condition, 'order_id,order_type,create_time, order_money');
        $group_order_list = $group_order_list['data'];

        foreach ($date_day as $item){
            $group_order_data[$item] = 0;
            $sms_order_data[$item] = 0;
            $group_order_money[$item] = 0;
            $sms_order_money[$item] = 0;
            foreach ($group_order_list as $k => $v) {
                $time = date('Y-m-d', $v['create_time']);
                if($item == $time && $v['order_type'] == 'group') {
                    $group_order_data[$item]++;
                    $group_order_money[$item] += $v['order_money'];
                }
                if($item == $time && $v['order_type'] == 'sms') {
                    $sms_order_data[$item]++;
                    $sms_order_money[$item] += $v['order_money'];
                }
            }
        }

        $day_data = [];
        $group_order = [];
        $sms_order = [];
        $group_order_sum = [];
        $sms_order_sum = [];
        foreach ($group_order_data as $k => $v){
            $day_data[] = $k;
            $group_order[] = $v;
        }
        foreach ($sms_order_data as $k => $v){
            $sms_order[] = $v;
        }
        foreach ($group_order_money as $k => $v){
            $group_order_sum[] =sprintf('%.2f', round(floor($v * 100) / 100, 2));
        }
        foreach ($sms_order_money as $k => $v){
            $sms_order_sum[] = sprintf('%.2f', round(floor($v * 100) / 100, 2));
        }

        $this->assign('day_data', $day_data);
        $this->assign('group_order', $group_order);
        $this->assign('sms_order', $sms_order);
        $this->assign('group_order_money', $group_order_sum);
        $this->assign('sms_order_money', $sms_order_sum);

		return $this->fetch('sysorder/index');
	}
	
	/**
	 * 列表
	 */
	public function lists()
	{
		$order_type = input('order_type', '');
		$sys_order_model = new SysOrderModel();
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			
			$condition = [];
			$condition[] = [ 'is_delete', '=', 0 ];
			$search_type = input('search_type', '');
			$search_text = input('search_text', '');
			if (!empty($search_text)) {
				$condition[] = [ $search_type, 'LIKE', '%' . $search_text . '%' ];
			}
			
			if ($order_type) {
				$condition[] = [ 'order_type', '=', $order_type ];
			}
			
			$order_status = input('order_status', '');
			if ($order_status !== '') {
				$condition[] = [ 'order_status', '=', $order_status ];
			}
			
			$pay_type = input('pay_type', '');
			if ($pay_type) {
				$condition[] = [ 'pay_type', '=', $pay_type ];
			}
			$start_time = input('start_time', '');
			$end_time = input('end_time', '');
			if ($start_time && !$end_time) {
				$condition[] = [ 'create_time', '>=', date_to_time($start_time) ];
			} elseif (!$start_time && $end_time) {
				$condition[] = [ 'create_time', '<=', date_to_time($end_time) ];
			} elseif ($start_time && $end_time) {
				$condition[] = [ 'create_time', 'between', [ date_to_time($start_time), date_to_time($end_time) ] ];
			}
			
			$res = $sys_order_model->getSysOrderPageList($condition, $page, $page_size);
			return $res;
			
		} else {
			//订单类型
			$order_type_list = $sys_order_model->getOrderType();
			$this->assign('order_type_list', $order_type_list);
			//支付方式
			$pay_type_list = $sys_order_model->getPayType();
			$this->assign('pay_type_list', $pay_type_list);
			//订单状态
			$order_status = $sys_order_model->getOrderStatus();
			$this->assign('order_status', $order_status);
			$this->assign('order_type', $order_type);
			return $this->fetch('sysorder/lists');
		}
		
	}
	
	/**
	 * 详情
	 */
	public function detail()
	{
		$order_id = input('order_id', 0);
		$sys_order_model = new SysOrderModel();
		$alias = 'so';
		$field = 'so.*,s.expire_time,s.sms_num';
		$join = [
		    [ 'site s','so.site_id = s.site_id','left']
        ];

		$order_info = $sys_order_model->getSysOrderInfo([ [ 'so.order_id', '=', $order_id ] ], $field, $alias, $join);

		$this->assign('info', $order_info['data']);
		return $this->fetch('sysorder/detail');
	}
	
	/**
	 * 支付审核
	 */
	public function status()
	{
		$order_id = input('order_id', 0);
		$sys_order_model = new SysOrderModel();
		$order_info = $sys_order_model->getSysOrderInfo([ [ 'order_id', '=', $order_id ] ]);
		$this->assign('info', $order_info['data']);
		return $this->fetch('sysorder/status');
	}
	
	/**
	 * 审核通过
	 */
	public function pass()
	{
		if (request()->isAjax()) {
			$order_id = input('order_id');
			$sys_order_model = new SysOrderModel();
			$res = $sys_order_model->offlinePay($order_id);
			return $res;
		}
	}
	
	/**
	 * 审核拒绝
	 */
	public function refuse()
	{
		if (request()->isAjax()) {
			$order_id = input('order_id');
			$sys_order_model = new SysOrderModel();
			$res = $sys_order_model->refuseOfflinePay($order_id);
			return $res;
		}
	}
}