<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\admin\controller;

use app\model\message\SmsPackage as SmsPackageModel;

/**
 * 权限组 控制器
 * 版本 1.0.6
 */
class Syssmspackage extends BaseAdmin
{
	/**
	 * 列表
	 */
	public function lists()
	{
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$condition = [];
			$status = input('status', 'all');
			$search_keys = input('search_keys', "");
			if (!empty($search_keys)) {
				$condition[] = [ 'sms_name', 'like', '%' . $search_keys . '%' ];
			}
			if ($status != 'all' && $status != '') {
				$condition['status'] = [ 'status', '=', $status ];
			}
			$sms_package_model = new SmsPackageModel();
			$list = $sms_package_model->getSmsPackagePageList($condition, $page, $page_size, "create_time desc");
			return $list;
		}
		return $this->fetch('syssmspackage/lists');
	}
	
	/**
	 * 添加
	 * @return mixed
	 */
	public function add()
	{
		if (request()->isAjax()) {
			$data = [
				'sms_name' => input('sms_name', ''),
				'sms_image' => input('sms_image', ''),
				'sms_num' => input('sms_num', 0),
				'sms_money' => input('sms_money', 0),
				'status' => input('status', 0),
				'desc' => input('desc', ''),
				'create_time' => time()
			];
			$sms_package_model = new SmsPackageModel();
			$res = $sms_package_model->addSmsPackage($data);
			return $res;
		} else {
			return $this->fetch('syssmspackage/add');
		}
	}
	
	/**
	 * 编辑
	 * @return mixed
	 */
	public function edit()
	{
		$package_id = input('package_id', 0);
		$condition = array(
			[ 'package_id', '=', $package_id ]
		);
		$sms_package_model = new SmsPackageModel();
		if (request()->isAjax()) {
			$data = [
				'sms_name' => input('sms_name', ''),
				'sms_image' => input('sms_image', ''),
				'sms_num' => input('sms_num', 0),
				'sms_money' => input('sms_money', 0),
				'status' => input('status', 0),
				'desc' => input('desc', ''),
				'modify_time' => time()
			];
			$res = $sms_package_model->editSmsPackage($data, $condition);
			return $res;
		} else {
			$sms_package_info_result = $sms_package_model->getSmsPackageInfo($condition);
			$this->assign('info', $sms_package_info_result['data']);
			return $this->fetch('syssmspackage/edit');
		}
		
	}
	
	/**
	 * 删除
	 */
	public function delete()
	{
		if (request()->isAjax()) {
			$package_id = input('package_id', 0);
			$sms_package_model = new SmsPackageModel();
			$res = $sms_package_model->deleteSmsPackage([ [ 'package_id', '=', $package_id ] ]);
			return $res;
		}
	}
}