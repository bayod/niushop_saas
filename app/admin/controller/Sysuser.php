<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\admin\controller;


use app\model\system\SysUser as SysUserModel;
use app\model\system\SiteGroup as SiteGroupModel;
use app\model\system\SysOrder as SysOrderModel;
use app\model\system\Site;
use think\facade\Cache;

/**
 * 管理员 控制器
 * 版本 1.0.6
 */
class Sysuser extends BaseAdmin
{
	/**
	 * 用户列表
	 */
	public function lists()
	{
        $test = 'test';

		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$condition = [];
			$search_keys = input('search_keys', "");
			if (!empty($search_keys)) {
				$condition[] = [ 'username', 'like', '%' . $search_keys . '%' ];
			}
			$user_model = new SysUserModel();
			$list = $user_model->getUserPageList($condition, $page, $page_size, "create_time desc");
			return $list;
		} else {
			$this->forthMenu();
			$cert = Cache::get("niushop_cert");
			$this->assign("auth_cert", $cert);
			return $this->fetch('sysuser/lists');
		}
	}
	
	/**
	 * 添加用户
	 * @return mixed
	 */
	public function add()
	{
		if (request()->isAjax()) {
			$username = trim(input("username", ""));
			$password = trim(input("password", ""));
			$mobile = trim(input('mobile', ''));
			$data = array(
				'username' => $username,
				'password' => $password,
				'mobile' => $mobile,
			);
			$user_model = new SysUserModel();
			$result = $user_model->addUser($data);
			return $result;
		} else {
			return $this->fetch('sysuser/add');
		}
	}
	
	/**
	 * 编辑用户
	 * @return mixed
	 */
	public function edit()
	{
		$uid = input("sys_uid", 0);
		$user_model = new SysUserModel();
		if (request()->isAjax()) {
			$mobile = trim(input('mobile', ''));
			
			$condition = array(
				[ "sys_uid", "=", $uid ],
			);
			$data = array(
				'mobile' => $mobile,
			);
			$result = $user_model->editUser($data, $condition);
			return $result;
		} else {
			$condition = array(
				[ "sys_uid", "=", $uid ],
			);
			$user_info_result = $user_model->getUserInfo($condition);
			$user_info = $user_info_result["data"];
			$this->assign("user_info", $user_info);
			return $this->fetch('sysuser/edit');
		}
	}
	
	/**
	 * 用户详情
	 * @return array|mixed
	 */
	public function detail()
	{
		$sys_uid = input('sys_uid');
		
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$condition[] = [ 'buyer_uid', '=', $sys_uid ];
			$condition[] = [ 'is_delete', '=', 0 ];
			$start_time = input('start_time', '');
			$end_time = input('end_time', '');
			if ($start_time && $end_time) {
				$condition[] = [ 'create_time', 'between', [ $start_time, $end_time ] ];
			} elseif (!$start_time && $end_time) {
				$condition[] = [ 'create_time', '<=', $end_time ];
			} elseif ($start_time && !$end_time) {
				$condition[] = [ 'create_time', '>=', $start_time ];
			}
			
			$pay_status = input('pay_status');
			if ($pay_status !== null) {
				$condition[] = [ 'pay_status', '=', $pay_status ];
			}
			$sys_order_model = new SysOrderModel();
			$order_list = $sys_order_model->getSysOrderPageList($condition, $page, $page_size);
			return $order_list;
			
		} else {
			$this->assign('sys_uid', $sys_uid);
			$user_model = new SysUserModel();
			$user_info = $user_model->getUserInfo([ [ 'sys_uid', '=', $sys_uid ] ]);
			$this->assign('user_info', $user_info['data']);
			
			$this->forthMenu([ 'sys_uid' => $sys_uid ]);
			return $this->fetch('sysuser/detail');
		}
		
	}
	
	public function shopLists()
	{
		$sys_uid = input('sys_uid');
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			
			$order = 'site_id desc';
			
			$shop_model = new Site();
			return $shop_model->getSitePageList([ [ 'sys_uid', '=', $sys_uid ] ], $page, $page_size, $order, '*');
			
		} else {
			
			$this->assign('sys_uid', $sys_uid);
			$this->forthMenu([ 'sys_uid' => $sys_uid ]);
			return $this->fetch('sysuser/shop_lists');
		}
	}
	
	/**
	 * 重置密码
	 */
	public function modifyPassword()
	{
		if (request()->isAjax()) {
			$password = trim(input('password', '123456'));
			$uid = input('sys_uid', 0);
			$user_model = new SysUserModel();
			return $user_model->modifyUserPassword($password, [ [ 'sys_uid', '=', $uid ] ]);
		}
	}
	
	/**
	 * 删除用户
	 */
	public function delete()
	{
		if (request()->isAjax()) {
			$uid = input("sys_uid", 0);
			$user_model = new SysUserModel();
			$condition = array(
				[ "sys_uid", "=", $uid ],
                [ "is_admin", "=", 0 ]
			);
			$result = $user_model->deleteUser($condition);
			return $result;
		}
	}
	
	/**
	 * 创建店铺
	 */
	public function addSite()
	{
		if (request()->isAjax()) {
			
			$expire_time = input('expire_time', '');
			$data = [
				'sys_uid' => input('sys_uid', 0),
				'site_name' => input('site_name', ''),
				'group_id' => input('group_id', 0),
				'expire_time' => strtotime($expire_time),
				'is_try' => 0
			];
			
			$site_model = new Site();
			$res = $site_model->addSite($data);
			return $res;
		} else {
			$sys_uid = input('sys_uid', 0);
			$this->assign('sys_uid', $sys_uid);
			
			$user_model = new SysUserModel();
			$user_info = $user_model->getUserInfo([ [ 'sys_uid', '=', $sys_uid ] ]);
			$this->assign('user_info', $user_info['data']);
			
			//套餐列表
			$site_group_model = new SiteGroupModel();
			$group_list = $site_group_model->getSiteGroupList([ [ 'status', '=', 1 ] ]);
			$this->assign('group_list', $group_list['data']);
			return $this->fetch('sysuser/add_site');
		}
	}
}

