<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\admin\controller;

use app\model\message\MessageTemplate as MessageTemplateModel;
use app\model\message\Sms;

/**
 * 消息管理 控制器
 * 版本 1.0.5
 */
class Message extends BaseAdmin
{
	/**
	 *  消息管理 列表
	 */
	public function lists()
	{
		$message_model = new MessageTemplateModel();
		//买家消息
		$member_message_list_result = $message_model->getMessageTemplateList([ [ "message_type", "=", 1 ] ]);
		$member_message_list = $member_message_list_result["data"];
		$this->assign("member_message_list", $member_message_list);
		//卖家通知
		$shop_message_list_result = $message_model->getMessageTemplateList([ [ "message_type", "=", 2 ] ]);
		$shop_message_list = $shop_message_list_result["data"];
		$this->assign("shop_message_list", $shop_message_list);
		return $this->fetch("message/lists");
	}
	
	/**
	 * 编辑短信模板(跳转)
	 */
	public function editSmsMessage()
	{
		$keywords = input("keywords", '');
		$sms_model = new Sms();
		$edit_data_result = $sms_model->doEditSmsMessage();
		if (empty($edit_data_result["data"][0]))
			$this->error("没有开启的短信方式!");
		
		$edit_data = $edit_data_result["data"][0];
		$edit_url = $edit_data["edit_url"];
		$this->redirect(addon_url($edit_url, [ "keywords" => $keywords ]));
	}
	
	/**
	 * 短信列表
	 */
	public function sms()
	{
		if (request()->isAjax()) {
			$sms_model = new Sms();
			$list = $sms_model->getSmsType(0);
			return $list;
		} else {
			$this->forthMenu();
			return $this->fetch("message/sms");
		}
	}
	
	/**
	 * 短信记录
	 */
	public function smsRecords()
	{
		if (request()->isAjax()) {
			$sms_model = new Sms();
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$search_text = input('search_text', '');
			$status = input('status', 'all');
			$condition = [];
			if (!empty($search_text)) {
				$condition[] = [ "keywords_name", "like", "%" . $search_text . "%" ];
			}
			if (!empty($status) && $status != 'all') {
				if ($status == -1) {
					$condition[] = [ 'status', 'not in', [ 0, 1, '' ] ];
				} else {
					$condition[] = [ 'status', '=', $status - 1 ];
				}
			}
			
			$list = $sms_model->getSmsRecordsPageList($condition, $page, $page_size);
			return $list;
		} else {
			$this->forthMenu();
			return $this->fetch("message/smsrecords");
		}
	}
	
	/**
	 * 删除短信记录
	 */
	public function deleteSmsRecords()
	{
		if (request()->isAjax()) {
			$ids = input("ids", "");
			$sms_model = new Sms();
			$condition = array(
				[ "id", "in", $ids ]
			);
			$result = $sms_model->deleteSmsRecords($condition);
			return $result;
		}
	}
	
	public function test()
	{
		event("AddMemberAccount", [ "account_type" => 'growth', 'member_id' => 243, 'site_id' => 417 ]);
//        $order_id = input("order_id", "");
//        $code = str_pad(random_int(1, 9999), 4, 0, STR_PAD_LEFT);// 生成4位随机数，左侧补0
//        $message_model = new \app\model\message\Message();
//        $res = $message_model->sendMessage([ "member_id" => 234, "site_id" => 417, "number" => $code, "support_type" => [ "sms" ], "keywords" => "MEMBER_PAY_PASSWORD" ]);
	}
}