<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\home\controller;

use app\model\system\Addon;
use app\model\system\SiteGroup;
use app\model\system\SiteGroup as SiteGroupModel;
use app\model\system\SysOrder as SysOrderModel;

class Sysusergroup extends BaseHome
{

    /**
     * 首页
     * @return mixed
     */
    public function index()
    {

        $condition = [['status', '=', 1]];

        $addon = new Addon();
        $install_result = $addon->getAddonList([["type", "<>", "system"]], "name,title");
        $install = $install_result["data"];
        $install_list = array_column($install, "title", "name");
        $sys_user_group_model = new SiteGroup();
        $list = $sys_user_group_model->getSiteGroupList($condition, '', "fee asc");
        if (!empty($list["data"])) {
            foreach ($list["data"] as $k => $v) {
                $temp_array = explode(",", $v['addon_array']);
                $exist_addon_array = [];
                $noexist_addon_array = [];
                foreach($install_list as $temp_k => $temp_v){
                    if(in_array($temp_k, $temp_array)){
                        $exist_addon_array[] = $temp_v;
                    }else{
                        $noexist_addon_array[] = $temp_v;
                    }
                }
                $list["data"][$k]["exist_addon_list"] = $exist_addon_array;
                $list["data"][$k]["noexist_addon_list"] = $noexist_addon_array;
            }
        }

        $this->assign('addon_list',$list['data']);
        return $this->fetch('sysusergroup/index');
    }

    /**
     * 详情
     */
    public function detail()
    {
        $group_id = input('group_id', 0);
        $sys_user_group_model = new SiteGroupModel();
        $group_info = $sys_user_group_model->getSiteGroupDetail([['group_id', '=', $group_id], ['status', '=', 1]]);
        if (empty($group_info['data'])) {
            $this->error(-1, '当前权限组已下架或者已被删除！');
        }
        $addon = new Addon();
        $install = $addon->getAddonList();
        $addon_array = $group_info['data']['addon_array'];
        $addon_array = explode(',', $addon_array);
        foreach ($install['data'] as $key => $v) {
            if (!in_array($v['name'], $addon_array)) {
                unset($install['data'][$key]);
            }
        }
        $this->assign('addons', $install['data']);
        $this->assign('info', $group_info['data']);
        return $this->fetch('sysusergroup/detail');
    }


    /**
     * 订单购买页
     */
    public function payment(){
        //关闭未完成的套餐订单
        $sys_order_model = new SysOrderModel();
        $condition = array(
            ["pay_status", "=", 0],
            ["buyer_uid", '=', $this->sys_uid],
            ["order_type", 'in', ['group', 'site']],
        );
        $close_result = $sys_order_model->orderClose($condition);

        $group_id = input('group_id', 0);
        $sys_user_group_model = new SiteGroup();
        $group_info_result = $sys_user_group_model->getSiteGroupDetail([['group_id', '=', $group_id], ['status', '=', 1]]);
        $group_info = $group_info_result['data'];
        if (empty($group_info)) {
            $this->error(-1, '当前权限组已下架或者已被删除！');
        }

        $this->assign('info', $group_info);
        return $this->fetch('sysusergroup/payment');
    }



    /**
     * 是否存在待支付的套餐订单
     */
    public function checkOrder(){
        $group_id = input('group_id', 0);
        $sys_order_model = new SysOrderModel();
        $condition = array(
            ["pay_status", "=", 0],
//            ["group_id", "=", $group_id],
            ["buyer_uid", '=', $this->sys_uid],
            ["order_status",'=',0],
            ["order_type", '=', 'group'],
        );
        $result = $sys_order_model->getOrderInfo($condition, 'out_trade_no');
        return $result;
    }
}