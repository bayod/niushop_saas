<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\model\system;


use app\model\BaseModel;
use app\model\system\Config as ConfigModel;

/**
 * 客服配置
 */
class Servicer extends BaseModel
{
    /**
     * 设置客服配置
     * @param $data
     * @return array
     */
    public function setServicerConfig($data, $site_id, $app_module)
    {
        $config_model = new ConfigModel();
        $res = $config_model->setConfig($data, '客服配置', 1, [['site_id', '=', $site_id], ['app_module', '=', $app_module], ['config_key', '=', 'SRRVICER_ROOT_CONFIG']]);
        return $res;
    }

    /**
     * 获取客服配置
     */
    public function getServicerConfig($site_id, $app_module)
    {
        $config_model = new ConfigModel();
        $res = $config_model->getConfig([['site_id', '=', $site_id], ['app_module', '=', $app_module], ['config_key', '=', 'SRRVICER_ROOT_CONFIG']]);
        $socket_url           = (get_http_type() === 'http' ? str_replace('http', 'ws', __ROOT__) : str_replace('https', 'wss', __ROOT__)) . '/wss';
        if (empty($res['data']['value'])) {
            $res['data']['value'] = [
                'system'     => 0,
                'weapp'      => 0,
                'open'       => 0,
                'open_pc'    => 0,
                'open_url'   => '',
                'socket_url' => $socket_url,
            ];
        }
        // 判断pc配置
        if(empty($res['data']['value']['open_pc'])){
            $res['data']['value']['open_pc'] = 0;
        }
        // 判断socket配置
        if(empty($res['data']['value']['socket_url'])){
            $res['data']['value']['socket_url'] = $socket_url;
        }
        return $res;
    }
}
