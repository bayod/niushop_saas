<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\model\system;

use app\model\BaseModel;

/**
 * 系统订单统计
 */
class SysOrderStat extends BaseModel
{
	
	/**
	 * 订单数量
	 * @param array $condition
	 */
	public function getOrderCount($condition = [])
	{
		$count = model('sys_order')->getCount($condition, 'order_id');
		return $this->success($count);
	}
	
	/**
	 * 订单总额
	 * @param array $condition
	 */
	public function getOrderSum($condition = [], $field)
	{
		$count = model('sys_order')->getSum($condition, $field);
		return $this->success($count);
	}

    /**
     * 订单列表
     * @param array $condition
     */
    public function getOrderList($condition = [], $field)
    {
        $list = model('sys_order')->getList($condition, $field);
        return $this->success($list);
    }
	
	
}