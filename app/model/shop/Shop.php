<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\model\shop;

use app\model\BaseModel;
use app\model\system\Config as ConfigModel;

/**
 * 店铺信息（无缓存）
 */
class Shop extends BaseModel
{

    /**
     * 修改店铺(不能随意修改组)
     * @param array $data
     */
    public function editShop($data, $condition)
    {
        $res = model('shop')->update($data, $condition);
        return $this->success($res);
    }

    /**
     * 删除店铺
     * @param $site_id
     * @return array
     */
    public function deleteShop($site_id)
    {
        model('shop')->startTrans();
        try {

            //获取系统用户id
            $sys_user = model('site')->getInfo([ [ 'site_id', '=', $site_id ] ], 'sys_uid');
            model('sys_user')->setDec([ [ 'sys_uid', '=', $sys_user[ 'sys_uid' ] ] ], 'created_site_num');
            //删除站点
            model('site')->delete([ [ 'site_id', '=', $site_id ] ]);
            //删除店铺
            model('shop')->delete([ [ 'site_id', '=', $site_id ] ]);
            //删除用户组
            model('group')->delete([ [ 'site_id', '=', $site_id ], [ 'app_module', '=', 'shop' ] ]);
            //删除用户
            model('user')->delete([ [ 'site_id', '=', $site_id ] ]);
            //删除商品
            model('goods')->delete([ [ 'site_id', '=', $site_id ] ]);
            model('goods_sku')->delete([ [ 'site_id', '=', $site_id ] ]);
            //删除订单
            model('order')->delete([ [ 'site_id', '=', $site_id ] ]);
            //删除会员
            model('member')->delete([ [ 'site_id', '=', $site_id ] ]);

            //执行事件
            event("DeleteShop", [ 'site_id' => $site_id ]);

            model('shop')->commit();

            return $this->success();
        } catch (\Exception $e) {

            model('shop')->rollback();
            return $this->error('', $e->getMessage());
        }
    }

    /**
     * 获取店铺信息
     * @param array $condition
     * @param string $field
     */
    public function getShopInfo($condition, $field = '*')
    {
        $res = model('shop')->getInfo($condition, $field);
        return $this->success($res);
    }

    /**
     * 获取店铺详情
     * @param int $site_id
     */
    public function getShopDetail($site_id)
    {
        $res = [];
        $shop_info = model('shop')->getInfo([ [ 'site_id', '=', $site_id ] ], 'site_id,expire_time,site_name,username,shop_status,logo,avatar,banner,seo_description,qq,ww,telephone,workingtime,email');

        $res [ 'shop_info' ] = $shop_info;

        return $this->success($res);
    }

    /**
     * 获取店铺列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param string $limit
     */
    public function getShopList($condition = [], $field = '*', $order = '', $limit = null)
    {

        $list = model('shop')->getList($condition, $field, $order, '', '', '', $limit);
        return $this->success($list);
    }

    /**
     * 获取店铺分页列表
     * @param array $condition
     * @param number $page
     * @param string $page_size
     * @param string $order
     * @param string $field
     */
    public function getShopPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '', $field = '*')
    {
        $join = [
            [
                'site ns1',
                'ns.site_id = ns1.site_id',
                'inner'
            ]
        ];
        $list = model('shop')->pageList($condition, $field, $order, $page, $page_size, 'ns', $join);

        return $this->success($list);
    }

    /**
     * 获取申请店铺数
     * @param array $condition
     */
    public function getShopCount($condition = [])
    {
        $res = model('shop')->getCount($condition);
        return $this->success($res);
    }

    /**
     * 店铺推广二维码
     * @param $site_id
     * @param string $type
     * @return array
     */
    public function qrcode($site_id, $type = "create")
    {
        $data = [
            'site_id' => $site_id,
            'app_type' => "all", // all为全部
            'type' => $type, // 类型 create创建 get获取
            'data' => [
                "site_id" => $site_id
            ],
            'page' => '/pages/index/index/index',
            'qrcode_path' => 'upload/qrcode/shop/',
            'qrcode_name' => "shop_qrcode_" . $site_id,
        ];

        event('Qrcode', $data, true);
        $app_type_list = config('app_type');

        $path = [];

        foreach ($app_type_list as $k => $v) {
            switch ( $k ) {
                case 'h5':
                    $wap_domain = getH5Domain($site_id);
                    $path[ $k ][ 'status' ] = 1;
                    $path[ $k ][ 'url' ] = $wap_domain . $data[ 'page' ] . '?site_id=' . $site_id;
                    $path[ $k ][ 'img' ] = $data[ 'qrcode_path' ] . $data[ 'qrcode_name' ] . '_' . $k . '.png';
                    break;
                case 'weapp' :
                    $config = new ConfigModel();
                    $res = $config->getConfig([ [ 'site_id', '=', $site_id ], [ 'app_module', '=', 'shop' ], [ 'config_key', '=', 'WEAPP_CONFIG' ] ]);
                    if (!empty($res[ 'data' ])) {
                        if (empty($res[ 'data' ][ 'value' ][ 'qrcode' ])) {
                            $path[ $k ][ 'status' ] = 2;
                            $path[ $k ][ 'message' ] = '未配置微信小程序';
                        } else {
                            $path[ $k ][ 'status' ] = 1;
                            $path[ $k ][ 'img' ] = $res[ 'data' ][ 'value' ][ 'qrcode' ];
                        }

                    } else {
                        $path[ $k ][ 'status' ] = 2;
                        $path[ $k ][ 'message' ] = '未配置微信小程序';
                    }
                    break;

                case 'wechat' :
                    $config = new ConfigModel();
                    $res = $config->getConfig([ [ 'site_id', '=', $site_id ], [ 'app_module', '=', 'shop' ], [ 'config_key', '=', 'WECHAT_CONFIG' ] ]);
                    if (!empty($res[ 'data' ])) {
                        if (empty($res[ 'data' ][ 'value' ][ 'qrcode' ])) {
                            $path[ $k ][ 'status' ] = 2;
                            $path[ $k ][ 'message' ] = '未配置微信公众号';
                        } else {
                            $path[ $k ][ 'status' ] = 1;
                            $path[ $k ][ 'img' ] = $res[ 'data' ][ 'value' ][ 'qrcode' ];
                        }
                    } else {
                        $path[ $k ][ 'status' ] = 2;
                        $path[ $k ][ 'message' ] = '未配置微信公众号';
                    }
                    break;
            }

        }
        $return = [
            'path' => $path,
        ];
        return $this->success($return);
    }


    /**
     * 设置店铺状态
     * @param $data
     * @param $site_id
     * @param $app_module
     * @return array
     */
    public function setShopStatus($data, $site_id, $app_module)
    {
        $config = new ConfigModel();
        $res = $config->setConfig($data, '商城状态设置', 1, [ [ 'site_id', '=', $site_id ], [ 'app_module', '=', $app_module ], [ 'config_key', '=', 'SHOP_STATUS_CONFIG' ] ]);
        return $res;
    }

    /**
     * 获取店铺状态
     * @param $site_id
     * @param $app_module
     * @return array
     */
    public function getShopStatus($site_id, $app_module)
    {
        $config = new ConfigModel();
        $res = $config->getConfig([ [ 'site_id', '=', $site_id ], [ 'app_module', '=', $app_module ], [ 'config_key', '=', 'SHOP_STATUS_CONFIG' ] ]);
        if (empty($res[ 'data' ][ 'value' ])) {
            $res[ 'data' ][ 'value' ] = [
                'shop_status' => 1
            ];
        }
        return $res;
    }
}