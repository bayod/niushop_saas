<?php
/**
 * NiuShop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 */

namespace addon\niusms\event;

use addon\niusms\model\Config;
use app\model\message\Message as MessageModel;

/**
 * 获取短信模板数据
 */
class EditMessage
{
    /**
     * 获取短信模板数据
     */
    public function handle($param)
    {
        $config_model = new Config();
        $sms_config = $config_model->getSmsConfig();
        $sms_config = $sms_config[ 'data' ];

        if ($sms_config['is_use']) {
            $template_info = model('sms_template')->getInfo([ ['keywords', '=', $param['keywords'] ]]);

            $message_model = new MessageModel();
            if($template_info['status'] == 1 && $template_info['audit_status'] == 2){

                $res = $message_model->editMessage(['sms_is_open' => $param['sms_is_open'], 'keywords' => $param['keywords'], 'site_id' => $param['site_id'] ], [
                    ["keywords", "=", $param['keywords']],
                    ['site_id', '=', $param['site_id']],
                ]);
                return $res;
            }else if($param['sms_is_open'] == 0){
                $res = $message_model->editMessage(['sms_is_open' => $param['sms_is_open'], 'keywords' => $param['keywords'], 'site_id' => $param['site_id'] ], [
                    ["keywords", "=", $param['keywords']],
                    ['site_id', '=', $param['site_id']],
                ]);
                return $res;
            }
        }
    }
}