<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\wechatpay\model;

use app\model\system\Config as ConfigModel;
use app\model\BaseModel;

/**
 * 微信支付配置
 * 版本 1.0.4
 */
class Config extends BaseModel
{

    /**
     * 设置支付配置
     * @param $data
     * @param int $site_id
     * @param string $app_module
     * @return array
     */
    public function setPayConfig($data, $site_id = 0, $app_module = 'admin')
    {
        $config = new ConfigModel();
        if ($site_id == 0) {
            $app_module = 'admin';
        } else {
            $app_module = 'shop';
        }
        $res = $config->setConfig($data, '微信支付配置', 1, [ [ 'site_id', '=', $site_id ], [ 'app_module', '=', $app_module ], [ 'config_key', '=', 'WECHAT_PAY_CONFIG' ] ]);
        return $res;
    }

    /**
     * 获取支付配置
     * @param int $site_id
     * @param string $app_module
     * @return array
     */
    public function getPayConfig($site_id = 0, $app_module = 'admin')
    {
        $config = new ConfigModel();
        if ($site_id == 0) {
            $app_module = 'admin';
        } else {
            $app_module = 'shop';
        }
        $res = $config->getConfig([ [ 'site_id', '=', $site_id ], [ 'app_module', '=', $app_module ], [ 'config_key', '=', 'WECHAT_PAY_CONFIG' ] ]);
        return $res;
    }
}