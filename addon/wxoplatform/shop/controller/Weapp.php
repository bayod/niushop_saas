<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+
namespace addon\wxoplatform\shop\controller;

use addon\weapp\model\Config as WeappConfig;
use addon\wxoplatform\model\OpenPlatform;
use app\model\system\Api;
use app\shop\controller\BaseShop;
use addon\wxoplatform\model\Weapp as WeappModel;

/**
 * 微信开放平台基础功能
 */
class Weapp extends BaseShop
{
    protected $replace = [];    //视图输出字符串内容替换    相当于配置文件中的'view_replace_str'

    public function __construct()
    {
        parent::__construct();
        $this->replace = [
            'WEAPP_CSS' => __ROOT__ . '/addon/wxoplatform/shop/view/public/css',
            'WEAPP_JS' => __ROOT__ . '/addon/wxoplatform/shop/view/public/js',
            'WEAPP_IMG' => __ROOT__ . '/addon/wxoplatform/shop/view/public/img',
            'WEAPP_SVG' => __ROOT__ . '/addon/wxoplatform/shop/view/public/svg',
        ];
    }

    /**
     * 小程序发布和审核
     */
    public function plugin()
    {
        $weapp_config_model = new WeappConfig();
        $config_result = $weapp_config_model->getWeappConfig($this->site_id);
        $config = $config_result[ 'data' ];
        $this->assign("config", $config[ 'value' ]);

        $open_platform_model = new OpenPlatform();
        $template_list_result = $open_platform_model->getTemplateList();
        if ($template_list_result[ 'code' ] == 0) {
            if (is_array($template_list_result[ 'data' ]) && count($template_list_result[ 'data' ]) > 1) array_multisort(array_column($template_list_result[ 'data' ], 'template_id'), SORT_DESC, $template_list_result[ 'data' ]);
        } else {
            $template_list_result[ 'data' ] = [];
        }
        $this->assign("template_list", $template_list_result[ 'data' ]);

        return $this->fetch('weapp/plugin', [], $this->replace);
    }

    /**
     * 微信小程序代码上传
     */
    public function commit()
    {
        if (request()->isAjax()) {
            $api_model = new Api();
            $api_config = $api_model->getApiConfig();
            $api_config = $api_config[ 'data' ];

            $open_platform_model = new OpenPlatform($this->site_id);
            $template_id = input('template_id', 0);
            $version = input('version', '');
            $socket_url = (strstr(ROOT_URL, 'https://') === false ? str_replace('http', 'ws', ROOT_URL) : str_replace('https', 'wss', ROOT_URL)) . '/wss';
            $data = array (
                'template_id' => $template_id,
                'ext_json' => [
                    'extEnable' => true,
                    'extAppid' => $open_platform_model->appid,
                    'ext' => [
                        'siteId' => $this->site_id,
                        'baseUrl' => __ROOT__,
                        'imgDomain' => __ROOT__,
                        'apiSecurity' => $api_config[ 'is_use' ] ?? 0,
                        'publicKey' => $api_config[ 'value' ][ 'public_key' ] ?? '',
                        'webSocket' => $socket_url
                    ]
                ],
                'user_version' => $version,
                'user_desc' => '上传时间' . date('Y-m-d H:i:s', time())
            );
            $result = $open_platform_model->weappCommit($data);
            return $result;
        }
    }

    /**
     * 微信审核
     */
    public function weappSubmitAudit()
    {
        if (request()->isAjax()) {
            $data = array ();
            $open_platform_model = new OpenPlatform();
            $result = $open_platform_model->weappSubmitAudit($data);
            return $result;
        }
    }

    /**
     * 获取体验二维码
     * @return array|mixed
     */
    public function getQrcode()
    {
        $open_platform_model = new OpenPlatform($this->site_id);
        $result = $open_platform_model->getQrcode();
        return $result;
    }

    /**
     * 审核记录
     */
    public function record()
    {
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $weapp_model = new WeappModel();
            $data = $weapp_model->getAuditPageList($this->site_id, [ [ 'site_id', '=', $this->site_id ] ], '*', 'id desc', $page, $page_size);
            return $data;
        }
        return $this->fetch('weapp/record', [], $this->replace);
    }

    /**
     * 体验者管理
     */
    public function experiencer()
    {
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $weapp_model = new WeappModel();
            $data = $weapp_model->getExperiencerPageList([ [ 'site_id', '=', $this->site_id ] ], '*', 'id desc', $page, $page_size);
            return $data;
        }
        return $this->fetch('weapp/experiencer', [], $this->replace);
    }

    /**
     * 绑定体验者
     * @return array|int|mixed|string
     */
    public function bindTestUser()
    {
        if (request()->isAjax()) {
            $wechatid = input('wechatid', '');
            $weapp_model = new WeappModel();
            $res = $weapp_model->bindExperiencer($this->site_id, $wechatid);
            return $res;
        }
    }

    /**
     * 解绑体验者
     * @return array|int|mixed|string
     */
    public function unbindTestUser()
    {
        if (request()->isAjax()) {
            $id = input('id', 0);
            $weapp_model = new WeappModel();
            $res = $weapp_model->unbindExperiencer($id, $this->site_id);
            return $res;
        }
    }

    /**
     * 撤回审核
     */
    public function undocodeaudit()
    {
        if (request()->isAjax()) {
            $id = input('id', 0);
            $weapp_model = new WeappModel();
            $res = $weapp_model->cancelAudit($id, $this->site_id);
            return $res;
        }
    }

    /**
     * 发布
     */
    public function release()
    {
        if (request()->isAjax()) {
            $id = input('id', 0);
            $weapp_model = new WeappModel();
            $res = $weapp_model->release($id, $this->site_id);
            return $res;
        }
    }

    public function test()
    {
        event('WeappAutoSubmit');
    }
}