<?php
// 事件定义文件
return [
    'bind'      => [
        
    ],

    'listen'    => [
        // 小程序自动提交审核
        'WeappAutoSubmit' => [
            'addon\wxoplatform\event\WeappAutoSubmit',
        ],
        // api配置变更
        'ApiConfigChange' => [
            'addon\wxoplatform\event\WeappAutoSubmit'
        ],
    ],

    'subscribe' => [
    ],
];