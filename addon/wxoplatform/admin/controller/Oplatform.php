<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+
namespace addon\wxoplatform\admin\controller;

use addon\wxoplatform\model\Config as ConfigModel;

use addon\wxoplatform\model\OpenPlatform;
use addon\wxoplatform\model\Weapp;
use app\admin\controller\BaseAdmin;

/**
 * 微信开放平台基础功能
 * 版本 1.0.4
 */
class Oplatform extends BaseAdmin
{

    /**
     * 开放平台配置
     */
    public function config()
    {
        $test = 'test';
        $config_model = new ConfigModel();
        if (request()->isAjax()) {
            $appid = input('appid', '');
            $secret = input('secret', '');
            $token = input('token', 'TOKEN');
            $aes_key = input('aes_key', '');
            $is_use = input('is_use', 0);
            $data = array(
                "appid" => $appid,
                "secret" => $secret,
                "token" => $token,
                "aes_key" => $aes_key,
            );

            $res = $config_model->setOplatformConfig($data, $is_use);
            return $res;
        } else {

            $config_result = $config_model->getOplatformConfig();
            $this->assign("config", $config_result["data"]);

            $request_scheme = $_SERVER['REQUEST_SCHEME'];
            $host = $_SERVER['HTTP_HOST'];
            $this->assign('request_scheme',$request_scheme);
            $this->assign('host',$host);
            $this->forthMenu();
            return $this->fetch('oplatform/config');
        }
    }

    /**
     * 小程序源码
     * @return mixed
     */
    public function sourceCode(){
        $is_new_version = 0;
        // 获取小程序版本信息
        $config_model = new ConfigModel();
        $version_info = $config_model->getWeappVersion();
        $version_info = $version_info['data']['value'];
        $currrent_version_info = config('info');
        if (!isset($version_info['version']) || (isset($version_info['version']) && $version_info['version'] != $currrent_version_info['version_no']) ) {
            $is_new_version = 1;
        }
        $this->assign('is_new_version', $is_new_version);

        $this->forthMenu();
        return $this->fetch('oplatform/source_code');
    }

    /**
     * 源码下载
     */
    public function download(){
        if (strstr(ROOT_URL, 'niuteam.cn') === false) {
            $weapp = new Weapp();
            $weapp->download();

            $config = new ConfigModel();
            $version_info = config('info');
            $config->setWeappVersion(['version' => $version_info['version_no'] ], 1);
        }
    }

    /**
     * 草稿箱
     */
    public function draft(){
        if (request()->isAjax()) {
            $open_platform = new OpenPlatform();
            $data = $open_platform->getDraftList();
            return $data;
        }
        $this->forthMenu();
        return $this->fetch('oplatform/draft');
    }

    /**
     * 添加到模板库
     * @return array
     */
    public function addTemplate(){
        if (request()->isAjax()) {
            $draft_id = input('draft_id', '');
            $open_platform = new OpenPlatform();
            $res = $open_platform->addToTemplate(['draft_id' => $draft_id]);
            return $res;
        }
    }

    /**
     * 删除模板
     * @return array
     */
    public function deleteTemplate(){
        if (request()->isAjax()) {
            $template_id = input('template_id', '');
            $open_platform = new OpenPlatform();
            $res = $open_platform->deleteTemplate(['template_id' => $template_id]);
            return $res;
        }
    }

    /**
     * 模板库
     */
    public function template(){
        if (request()->isAjax()) {
            $open_platform = new OpenPlatform();
            $data = $open_platform->getTemplateList();
            if ($data['code'] == 0) {
                if (is_array($data['data']) && count($data['data']) > 1) array_multisort(array_column($data['data'], 'template_id'), SORT_DESC, $data['data']);
                $template_info = $data['data'][0] ?? [];
                if (!empty($template_info)) {
                    $weapp = new Weapp();
                    $weapp->checkWeappVersionIsChange($template_info);
                }
            }
            return $data;
        }
        $this->forthMenu();
        return $this->fetch('oplatform/template');
    }

    public function test(){
        $weapp = new Weapp();
        $weapp->autoSubmitAudit();
    }

}