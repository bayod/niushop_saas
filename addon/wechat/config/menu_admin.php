<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+
return [

    [
        'name' => 'MESSAGE_WECHAT_EDIT',
        'title' => '编辑消息模板',
        'url' => 'wechat://admin/message/edit',
        'parent' => 'MESSAGE_LISTS',
        'is_show' => 0,
        'picture' => '',
        'picture_selected' => '',
    ]

];